﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace usingwebclient_listobjectesh.modelClas
{
    public class user
    {
        public int id { get; set; }
       public string name { get; set; }
       public string username { get; set; }
        public string email { get; set; }
        List<string> addresses { get; set; }
        List<float> geo { get; set; }
        public string phone { get; set; }
        public string website { get; set; }
        List<string> company { get; set; }
    }
}
